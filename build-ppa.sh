#!/bin/bash

build_package(){
	pkg_name="$(echo "$directory" | sed 's/-[0-9]\+\(\.[0-9]\+\)*$//')"

	debian/rules clean
	dir0="$(pwd)"

	for i in bionic cosmic disco
	do
		old_header=$(head -1 ./debian/changelog)
		old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
		new_version="${old_version}~${i}1"
		sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
		sed -i -re "1s/unstable/$i/" ./debian/changelog
		# -I to exclude .git; -d to allow building .changes file without build dependencies installed
		dpkg-buildpackage -I -S -sa -d
		sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
		cd ..
		
		# change PPA names to yours, you may leave only one PPA; I upload to 2 different PPAs at the same time
		#for ppa_name in ppa:mikhailnov/desktop1-dev #ppa:mikhailnov/utils
		for ppa_name in ppa:mikhailnov/utils
		# I can copy from desktop1-dev to utils at Launchpad
		do
			dput -f "$ppa_name" "$(/bin/ls -tr ${pkg_name}_${version}*_source.changes | tail -n 1)"
		done
		
		cd "$dir0"
		sleep 1
	done

	debian/rules clean
	#cd "$dir_start"
}

dir_start="$(pwd)"
for directory in "mlt-6.14.0" "kdenlive-18.12.3"
do
	cd "$directory"
	build_package #"$directory"
	cd "$dir_start"
done
