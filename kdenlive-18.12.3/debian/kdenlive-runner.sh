#!/bin/sh
# Prevent GTK+ file dialogs in non-KDE DEs
# https://nixtux.ru/654

if [ "$XDG_CURRENT_DESKTOP" = 'KDE' ]
	then kdenlive "$@"
	else env XDG_CURRENT_DESKTOP=rtpkgl kdenlive "$@"
fi
